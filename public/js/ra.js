// Click functions
$(document).on('click', '#editdocument', function() { editDocument() } );
$(document).on('click', '#addRisk', function() { addRisk() } );
$(document).on('click', '#submitHazard', function() { addHazard() } );
$(document).on('click', '.editcircle.four', function() { deleteHazard($(this).data("key")) } );
$(document).on('click', '.editcircle.one', function() { editHazard($(this).data("key")) } );
$(document).on('click', '.undodelete', function() { undoDelete($(this).data("key")) } );
$(document).on('click', '.controls.three', function() { undoMode() } );

//$(document).on('click', '.wiz', function() { revealStep($(this)) } );

$(document).on('click', '.hazardcategory', function() { revealScores($(this).data("key"), $(this)) } );
$(document).on('click', '.impactdescription', function() { revealFreq($(this)) } );
$(document).on('click', '.frequencydescription', function() { riskScores($(this)) } );
$(document).on('click', '.frequencydescriptioncontrol', function() { prepareSubmit($(this)); } );


var rs;
var cs;
var storageRef = firebase.storage().ref();

// variables for submitting hazard
var riskdescription;
var risktype; // assigned in revealScores()
var controldescription;
var riskimpact; // assigned in riskScores();
var riskfrequency; // assigned in revealFreq()
var controlfrequency; // assigned in prepareSubmit();


var assessment = getURLParameter("assessment");



function prepareSubmit(activeFrequency) {
	
	// store the control's frequency score
	controlfrequency = activeFrequency.data("frequencyscore");
	
	$('#submitHazard').addClass('button-secondary');
	
	$('.frequencydescriptioncontrol').removeClass("activatedTable");
	activeFrequency.addClass('activatedTable');
	
}

function gotoControl() {
	

	
	$('.wizcontent').addClass("hidden");
	$('.wiz').removeClass('active');
	
	$('.wizcontent.second').removeClass('hidden'); 
	$('.wiz.second').addClass('active');
	
	$('#submitHazard').removeClass('hidden');
	$('#gotocontrol').addClass('hidden');
	
	$('#gotorisk').removeClass('hidden');
	
	$(document).on('click', '#gotorisk', function() { gotoRisk() } );
}

function gotoRisk() {
	
	$('.wizcontent').addClass("hidden");
	$('.wiz').removeClass('active');
	
	$('.wizcontent.first').removeClass('hidden'); 
	$('.wiz.first').addClass('active');
	
	$('#submitHazard').addClass('hidden');
	$('#gotocontrol').removeClass('hidden');
	$('#gotorisk').addClass('hidden');
}


function riskScores(activeScore) {
	
	$('.frequencydescription').removeClass("activatedTable");
	activeScore.addClass('activatedTable');
	
	// Store the selected frequency's score
	riskfrequency = activeScore.data("frequencyscore");
	
	$('#gotocontrol').addClass('button-primary');
	
	// if form is valid then do this (XXX not done)
	$(document).on('click', '#gotocontrol', function() { gotoControl() } );
	
	
}

function revealStep(stepClicked) {
	$('.wizcontent').addClass("hidden");
	$('.wiz').removeClass('active');
	
	if ( stepClicked.hasClass("first") ) {	$('.wizcontent.first').removeClass('hidden'); $('.wiz.first').addClass('active'); }
	if ( stepClicked.hasClass("second") ) {	$('.wizcontent.second').removeClass('hidden'); $('.wiz.second').addClass('active'); }
	if ( stepClicked.hasClass("third") ) { $('.wizcontent.third').removeClass('hidden'); $('.wiz.third').addClass('active'); }
}



function revealScores(key, activeButton) {
	
	// Use the key to find the scores and descriptions for this specific risk category
	
	
	// Highlight that this has been selected
	$('.hazardcategory').removeClass("button-primary");
	activeButton.addClass("button-primary");
	
	// Store the risk category
	risktype = key;
	
	firebase.database().ref('/ra/settings/hazardcategories/' + key + '/scores').once('value').then(function(snapshot) {
		var scores = snapshot.val();
		var counter = 0;
		//console.log(scores);
		
		$('#scoreList').html('');
		
		if ( scores ) {
			$.each(scores, function(index, value) {
				counter = counter + 1;
				if ( scores[counter].description != "" ) {
					$('#scoreList').append('<tr class="impactdescription" data-riskscore="' + scores[counter].score + '"><td>' + scores[counter].description + '</td></tr>')
				}
			});
		} else {
			//handle errors 
		};
	});	
}

function revealFreq(activeCategory) {
	
	// Reveal frequencies on click
	$('.impactdescription').removeClass("activatedTable");
	activeCategory.addClass('activatedTable');
	
	// Store the selected description's score
	riskimpact = activeCategory.data("riskscore");
	
	firebase.database().ref('/ra/settings/frequencies/').once('value').then(function(snapshot) {
		var frequencies = snapshot.val();

		console.log(frequencies);
		$('#frequencyList').html('');
		$('#frequencyListforControl').html('');
		
		if ( frequencies ) {
			$.each(frequencies, function(index, value) {
				$('#frequencyList').append('<tr class="frequencydescription" data-frequencyscore="' + value.score + '"><td>' + value.description + '</td></tr>');
				$('#frequencyListforControl').append('<tr class="frequencydescriptioncontrol"  data-frequencyscore="' + value.score + '"><td>' + value.description + '</td></tr>')
			});
		} else {
			//handle errors 
		};
		
		
		
	});	
}




function editDocument() {
	
	gotoRisk();
	
	$('#editBar').removeClass('delayed');
	$('.editcontrol').removeClass('delayed');
	

	
	if ( $('#undoBar').hasClass('undoon') ) {
		$('#editBar').addClass('delayed');
		$('.editcontrol').addClass('delayed');
		undoMode();
	}
	
	$('.controls.one').toggleClass('hidden');
	$('.controls.two').toggleClass('hidden');
	$('.controls.three').toggleClass('hidden');
	
	$('.riskedit').toggleClass('hidden');
	$('.editCircle').toggleClass('hidden');
	
	
	$('#editBar').toggleClass('editon');
	$('.editcontrol').toggleClass('active');
	$('#closeeditBar').toggleClass('minimised');
	
	
	
	$('.hazard').hover( 
	
		function() { 
			$('.editcircle').addClass('hidden');
			$('div[data-key='    + $(this).data("key") +   ']').removeClass('hidden'); 
		},
		function() { 
			//$('.editcircle').addClass('hidden'); 
		}

	);
	
	
	$('.controledit').toggleClass('hidden');
	
	
	//$(document).on('click', '#showChangelog', function() { 
		//$( "#changelog" ).toggleClass("hidden");
	//});
	
	allowDragandDrop();
	
}

function undoMode() {
	$('#undoBar').toggleClass('undoon');
}


function allowDragandDrop() {
	// ALLOW DRAG AND DROP OF STEPS
	//$(".stepNumber").html('Hold to rearrange');	
	var adjustment;
	$("ol.hazards").sortable({  
			handle: ".editcircle.three", 
			onDrop: function  ($item, container, _super) {
			var $clonedItem = $('<li/>').css({height: 0});
			$item.before($clonedItem);
			$clonedItem.animate({'height': $item.height()});
			$item.animate($clonedItem.position(), function  () {
			  $clonedItem.detach();
			  _super($item, container);
			});
			//saveMode(true, true);
			//saveRearrange(true);
		  },
		  // set $item relative to cursor position
		  onDragStart: function ($item, container, _super) {
			var offset = $item.offset(),
				pointer = container.rootGroup.pointer;
			adjustment = {
			  left: pointer.left - offset.left,
			  top: pointer.top - offset.top
			};
			_super($item, container);
		  },
		  onDrag: function ($item, position) {
			$item.css({
			  left: position.left - adjustment.left,
			  top: position.top - adjustment.top
			});
			//$("li.placeholder").html("Step will be inserted in this position");
		  }
	});
};




function addRisk() {
	
	
	gotoRisk();
	
	
	// download a list of risk categories, and their scoring rates
	// populate some html elements to contain these, and add them to the modal
	
	
	firebase.database().ref('/ra/settings/hazardcategories/').once('value').then(function(snapshot) {
		var categories = snapshot.val();
		
		if ( categories ) {
			
			$('#categoryList').html('');
			
			$.each(categories, function(index, value) {
				$('#categoryList').append('<button class="u-full-width u-max-full-width hazardcategory" data-key="' + index + '">' + value.title + '</button>')
			});
		} else {
			//handle errors 
		};
		
		
		
	});
	
	
}


function addHazard() {
	
	
	var totalHaz = 0;
	
	
	riskdescription = $('#enterrisk').val();
	controldescription = $('#entercontrol').val();
	
	console.log(risktype);
	console.log(riskimpact);
	console.log(riskfrequency);
	console.log(riskdescription);
	console.log(controldescription);
	console.log(controlfrequency);
	
	
	firebase.database().ref('/ra/assessments/' + assessment).once('value').then(function(snapshot) {
		totalHaz = snapshot.val().hazcount;	
		
		var currentAuthor = firebase.auth().currentUser.email;
		var currentTime = moment().format('D MMM YYYY, HH:mm');
		var riskimage = false;
		
		if ( $( "input[name='slim[]']" ).val() ) { riskimage = true; };
		
		var newData = { 
			'risk': riskdescription, 
			'risktype': risktype,
			'control': controldescription, 
			'riskimpact': riskimpact,
			'riskfrequency': riskfrequency,
			'controlfrequency': controlfrequency,
			'deleted': false,
			'created': currentTime,
			'updated': currentTime,
			'lastedited': currentAuthor,
			'author': currentAuthor,
			'riskimage': riskimage
		}
	
		// Get a key for a new hazard
		var newKey = firebase.database().ref('/ra/' + assessment).child('hazards').push().key;

		// Write the new hazard data
		var updates = {};
		updates['/ra/assessments/' + assessment + '/hazards/' + newKey] = newData;
		updates['/ra/assessments/' + assessment + '/updated/'] = currentTime;
		updates['/ra/assessments/' + assessment + '/lastedited/'] = currentAuthor;
		updates['/ra/assessmentslist/list/' + assessment + '/updated/'] = currentTime;
		
		
		
		updates['/ra/assessments/' + assessment + '/hazcount/'] = totalHaz + 1;
		updates['/ra/assessmentslist/list/' + assessment + '/hazcount/'] = totalHaz + 1;
		
		// write to database
		firebase.database().ref().update(updates);
		
		if ( $( "input[name='slim[]']" ).val() ) { saveImage(newKey) };
		
		renderHazards();
		
		
		
	});
			
		
		
		
		
}

function deleteHazard(key) {
	
	var totalHaz = 0;
	
	firebase.database().ref('/ra/assessments/' + assessment).once('value').then(function(snapshot) {
		totalHaz = snapshot.val().hazcount;	

		var currentAuthor = firebase.auth().currentUser.email;
		var currentTime = moment().format('D MMM YYYY, HH:mm');
		
		// Write the new hazard data
		var updates = {};
		updates['/ra/assessments/' + assessment + '/hazards/' + key + '/deleted'] = true;
		updates['/ra/assessments/' + assessment + '/hazcount/'] = totalHaz - 1;
		updates['/ra/assessmentslist/list/' + assessment + '/hazcount/'] = totalHaz - 1;
		updates['/ra/assessmentslist/list/' + assessment + '/updated/'] = currentTime;
		
		// write to database
		firebase.database().ref().update(updates);
		
		renderHazards();
		
		$.notiny({ 
			text: '<span class="notification">Deleted hazard.</span><button data-key="' + key + '" class="undodelete button-primary">Undo Delete</button>', 
			position: 'fluid-top' ,
			clickhide: false,
			delay: 8000,
		});
	
	});

}

function undoDelete(key) {
	
	var totalHaz = 0;
	
	firebase.database().ref('/ra/assessments/' + assessment).once('value').then(function(snapshot) {
		totalHaz = snapshot.val().hazcount;	
		
		var currentAuthor = firebase.auth().currentUser.email;
		var currentTime = moment().format('D MMM YYYY, HH:mm');
		
		// Write the new hazard data
		var updates = {};
		updates['/ra/assessments/' + assessment + '/hazards/' + key + '/deleted'] = false;
		updates['/ra/assessments/' + assessment + '/hazcount/'] = totalHaz + 1;
		updates['/ra/assessmentslist/list/' + assessment + '/hazcount/'] = totalHaz + 1;
		updates['/ra/assessmentslist/list/' + assessment + '/updated/'] = currentTime;
		
		// write to database
		firebase.database().ref().update(updates);

		$('.notiny-base').remove();
		
						
		$.notiny({ 
			text: '<span class="notification">Undone.</span>', 
			position: 'fluid-top' 
		});
		
		
		renderHazards();
	
	});

}

function editHazard(key) {
	
		console.log(key);
		
		
		$('div[data-hazard=' + key + ']').attr("contenteditable", "true");
		

}










// Render steps
	function renderHazards() {
				
		firebase.database().ref('/ra/assessments/' + assessment + '/hazards/').orderByChild('order').once('value').then(function(snapshot) {
			
			var thisHazard = snapshot.val();
			
			function sortProperties(obj, sortedBy, isNumericSort, reverse) {
				sortedBy = sortedBy || 1; // by default first key
				isNumericSort = isNumericSort || false; // by default text sort
				reverse = reverse || false; // by default no reverse
				var reversed = (reverse) ? -1 : 1;
				var sortable = [];
				for (var key in obj) {
					if (obj.hasOwnProperty(key)) {
						sortable.push([key, obj[key]]);
					}
				}
				if (isNumericSort)
					sortable.sort(function (a, b) {
						return reversed * (a[1][sortedBy] - b[1][sortedBy]);
					});
				else
					sortable.sort(function (a, b) {
						var x = a[1][sortedBy].toLowerCase(),
							y = b[1][sortedBy].toLowerCase();
						return x < y ? reversed * -1 : x > y ? reversed : 0;
					});
				return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
			}
			
			var sortedData = sortProperties(thisHazard, 'riskimpact', true, true);
			
			$("#hazardList").html("");
			
			for ( i = 0 ; i < sortedData.length ; i++ ) {
				
				var key = sortedData[i][0];
				var order = sortedData[i][1].order;
				var risk = sortedData[i][1].risk;
				var control = sortedData[i][1].control;
				var deleted = sortedData[i][1].deleted;
				var riskscore = sortedData[i][1].riskfrequency * sortedData[i][1].riskimpact;
				var controlscore = sortedData[i][1].riskimpact * sortedData[i][1].controlfrequency;
				var riskimage = sortedData[i][1].riskimage;
				var riskfrequency = sortedData[i][1].riskfrequency;
				var riskimpact = sortedData[i][1].riskimpact;
				
				if (!deleted) { 
					//counter = counter + 1;
					drawHazard(key, risk, control, riskscore, controlscore, riskimage, riskimpact, riskfrequency);
				};
				
			};
			
			
			$('.numberscore').each(function(index) {
				d3.select(this).style("background", colourScale(d3.select(this).text()));
			});
			

		});
		

		function drawHazard(key, risk, control, riskscore, controlscore, riskimage, riskimpact, riskfrequency)  {
			$("#hazardList").append('<li>\
				<div class="row hazard" data-key="' + key + '">\
					<div class="one-half column riskcontent entry">\
						<div class="one column scorebox">\
							<div class="row"><div class="numberscore">' + riskscore + '</div></div><br>\
							<div class="row"><div class="six columns">i: ' + riskimpact + '</div><div class="six columns">f: ' + riskfrequency + '</div></div>\
						</div>\
						<div class="one column" style="padding-left: 10px;"><div id="image' + key + '"></div></div>\
						<div class="ten columns risktext" data-hazard="' + key + '">' + risk + '</div>\
						<div class="editcircle one animated bounceIn hidden" data-key="' + key + '"><div class="editbutton"><i class="ion-edit"></i></div></div>\
						<div class="editcircle two animated bounceIn hidden" data-key="' + key + '"><a href="#" data-remodal-target="addImage"><div class="editbutton"><i class="ion-image"></i></div></div></a>\
						<div class="editcircle three animated bounceIn hidden" data-key="' + key + '"><div class="editbutton"><i class="ion-arrow-move"></i></div></div>\
					</div>\
					<div class="one-half column controlcontent entry">\
						<div class="two columns scorebox u-pull-right">\
							<div class="numberscore">' + controlscore + '</div>\
						</div>\
						<div class="ten columns risktext" data-hazard="' + key + '">' + control + '</div> \
						<div class="editcircle animated bounceIn four hidden" data-key="' + key + '"><div class="editbutton"><i class="ion-close-circled"></i></div></div>\
					</div>\
				</div></li>');
				
				
			if ( riskimage ) { 	
				storageRef.child('ra/images/' + assessment + '/risk/' + key).getDownloadURL().then(function(url) {
						$('#image' + key).html('<a class="stepimg" href="' + url + '" data-lightbox="' + key + '"><img class="u-full-width" stepid="' + key +'" src="' + url + '" data-lightbox="' + key + '"></a>');
				}).catch(function(error) {

				});
			}
				
			$('.editcircle.one').tooltip({position: 'left center', content: 'edit this hazard'});
			$('.editcircle.two').tooltip({position: 'left center', content: 'edit image'});
			$('.editcircle.three').tooltip({position: 'left center', content: 'change order'});
			$('.editcircle.four').tooltip({position: 'right center', content: 'delete hazard'});
			
		};
		
		
		

		
		

		
		
	};
	
	
	function renderPage() {
				
		firebase.database().ref('/ra/assessments/' + assessment).once('value').then(function(snapshot) {
			
			var pageDetails = snapshot.val();
			
			$('#assessmenttitle').text(pageDetails.title);
			

			
		});
		
		
	};
















// START: AUTHORISATION
firebase.auth().onAuthStateChanged(function(user) {
	
	if (user) {
		// User is signed in.
		//var url = window.location.href;
		
	
		
		//renderSteps();
		//renderPeople();
		//$("#signoutLink").on("click", function() { firebase.auth().signOut(); } );
		//$("#editLink").on("click", function() { editMode(true) } );
		//renderChangelog();
		console.log("signed in");

	
	}
	else {
		// No user is signed in. Give them an option to.
		console.log("no-one is signed in")
		//$("#stepList").html('');
		//$(".packTitle").text('User not signed in - cannot display information');
		//$(".packDep").text('Unauthorised user');
		//$("#signoutLink").attr("href", "../login.html");
	}
});
// END: AUTHORISATION

// START: PAGE LOAD

	$(document).ready(function($) {
		
		// Tooltips
		$('.controls.one').tooltip({position: 'left center', content: 'add a new hazard'});
		$('.controls.two').tooltip({position: 'left center', content: 'save edits'});
		$('.controls.three').tooltip({position: 'left center', content: 'view changelog'});
		
		$("#riskentry").steps({
			headerTag: "h3",
			bodyTag: "section",
			transitionEffect: "slideLeft",
			enableFinishButton: true,
			enablePagination: true,
			enableAllSteps: true,
			titleTemplate: "#title#",
			cssClass: "tabcontrol"
		});
		
		$("#riskscore").ionRangeSlider({
			type: "single",
			min: 0,
			max: 10,
			from: 5,
			step: 1,
			grid: true,
			grid_snap: true,
			onChange: function (data) {	rs = data.from; }
		});
		
		$("#controlscore").ionRangeSlider({
			type: "single",
			min: 0,
			max: 10,
			from: 5,
			step: 1,
			grid: true,
			grid_snap: true,
			onChange: function (data) {	cs = data.from; }
		});
		
		renderPage();
		renderHazards();
		
		
		$('body').slimParse();
				
		
		
		
	});

// END: PAGE LOAD




function saveImage(stepid) {

	var file = base64toBlob(JSON.parse($( "input[name='slim[]']" ).val()).output.image.split(',')[1], JSON.parse($( "input[name='slim[]']" ).val()).output.image.split(",")[0].split(":")[1].split(";")[0]);
	var metadata = {'contentType': file.type};

	function base64toBlob(base64Data, contentType) {
		contentType = contentType || '';
		var sliceSize = 1024;
		var byteCharacters = atob(base64Data);
		var bytesLength = byteCharacters.length;
		var slicesCount = Math.ceil(bytesLength / sliceSize);
		var byteArrays = new Array(slicesCount);

		for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
			var begin = sliceIndex * sliceSize;
			var end = Math.min(begin + sliceSize, bytesLength);

			var bytes = new Array(end - begin);
			for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
				bytes[i] = byteCharacters[offset].charCodeAt(0);
			}
			byteArrays[sliceIndex] = new Uint8Array(bytes);
		}
		return new Blob(byteArrays, { type: contentType });
	}
				

	// Upload file and metadata to the object 'images/mountains.jpg'
	var uploadTask = storageRef.child('ra/images/' + assessment + '/risk/' + stepid).put(file, metadata);

	// Listen for state changes, errors, and completion of the upload.
	uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
	  function(snapshot) {
		// Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
		var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
		
		console.log('Upload is ' + progress + '% done');
		
		switch (snapshot.state) {
		  case firebase.storage.TaskState.PAUSED: // or 'paused'
			console.log('Upload is paused');
			break;
		  case firebase.storage.TaskState.RUNNING: // or 'running'
			//console.log('Upload is running');
			break;
		}
	  }, function(error) {
	  switch (error.code) {
		case 'storage/unauthorized':
		  // User doesn't have permission to access the object
		  break;

		case 'storage/canceled':
		  // User canceled the upload
		  break;

		case 'storage/unknown':
		  // Unknown error occurred, inspect error.serverResponse
		  break;
	  }
	}, function() {
	  // Upload completed successfully, now we can get the download URL
	  var downloadURL = uploadTask.snapshot.downloadURL;
	  
	  
	 // renderSteps();
	console.log(downloadURL);
	$('#image' + stepid).html('<a class="stepimg" href="' + downloadURL + '" data-lightbox="' + stepid + '"><img class="u-full-width" stepid="' + stepid +'" src="' + downloadURL + '" data-lightbox="' + stepid + '"></a>');
					
			
	});
	
	

}





var colourScale = d3.scale.linear()
    .domain([0, 15, 30])
    .range(["green", "yellow", "red"]);





// START: HELPER FUNCTIONS


// Pull parameters from URL
	function getURLParameter(name) {
	  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	};

// END: HELPER FUNCTIONS
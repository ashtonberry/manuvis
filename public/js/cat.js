/* This whole thing needs a lot of work, this is a major bodge.
Display of each risk category (called from database) needs to be handled properly, preferably with an accordion.
The accordion drops down, revealing the individual descriptions for each category, and the scores. This should be editable.
At the moment, it's assumed there are five risk categories. This needs to be a variable quantity.
The "lastupdated" section doesn't work.
 */

var key;




// START: CLICK TRIGGERS

	$(document).on('click', '#submitCategory', function () { addCategory() } );
	$(document).on('click', '.scoreedit', function () { editScores($(this).data('key')) });
	$(document).on('click', '#submitScores', function () { addScores() });
	$(document).on('click', '#submitFrequency', function () { addFrequency() });
	
// END: CLICK TRIGGERS

// START: PAGE LOAD

	$(document).ready(function($) {
		
		// stuff
		console.log("validation");
		
		
		renderCategories();
		renderFrequencies();
		
	});

// END: PAGE LOAD


function renderCategories() {
	
	firebase.database().ref('/ra/settings/hazardcategories/').once('value').then(function(snapshot) {
		var categories = snapshot.val();
		
		
		var theList = [];

		if ( categories ) {
			$.each(categories, function(index, value) {
				theList.push(
				{ 
					"Title": '<a href="#" data-remodal-target="scores" class="scoreedit" data-key="' + index + '"><span class="text-main text-semibold">' + value.title + '</span></a><br><small class="text-muted">Last updated: ' + value.updated + '</small>', 
					"Actions": '<a href="view.html?assessment=' + index + '"><i class="ion-eye tablecontrols"></i></a><a href="view.html?assessment=' + index + '#addHazard"><i class="ion-plus-round tablecontrols"></i></a><i class="ion-edit tablecontrols"></i><i class="ion-close tablecontrols"></i>',
					"key": index
				});
			});
		};

		
		$('#columns').columns({
			  data: theList,
			  schema: [
				  {"header":"Title", "key":"Title"},
				  {"header":"Actions", "key":"Actions"},
			  ],
			  sortBy: 'Title',
			  size: 10,
		});

		$('.ui-table-search').attr('placeholder', 'quick search by typing in the title').addClass("u-full-width");
		
	});
	

}


function renderFrequencies() {
	
	firebase.database().ref('/ra/settings/frequencies/').once('value').then(function(snapshot) {
		var frequencies = snapshot.val();
		
		
		var theList = [];

		if ( frequencies ) {
			$.each(frequencies, function(index, value) {
				theList.push(
				{ 
					"Title": '<a href="#" data-remodal-target="scores" class="scoreedit" data-key="' + index + '"><span class="text-main text-semibold">' + value.description + '</span></a><br><small class="text-muted">Last updated: ' + value.updated + '</small>', 
					"Actions": '<a href="view.html?assessment=' + index + '"><i class="ion-eye tablecontrols"></i></a><a href="view.html?assessment=' + index + '#addHazard"><i class="ion-plus-round tablecontrols"></i></a><i class="ion-edit tablecontrols"></i><i class="ion-close tablecontrols"></i>',
					"key": index
				});
			});
		};

		
		$('#frequency').columns({
			  data: theList,
			  schema: [
				  {"header":"Title", "key":"Title"},
				  {"header":"Actions", "key":"Actions"},
			  ],
			  sortBy: 'Title',
			  size: 10,
		});

		$('.ui-table-search').attr('placeholder', 'quick search by typing in the title').addClass("u-full-width");
		
	});
	

}



function addCategory() {
	
		var currentAuthor = firebase.auth().currentUser.email;
		var currentTime = moment().format('D MMM YYYY, HH:mm');
		
		var newData = { 
			'author': currentAuthor,
			'created': currentTime,
			'updated': currentTime,
			'deleted': false,
			'title': $('#categoryName').val()
		}
	
		// Get a key for a new hazard
		var newKey = firebase.database().ref('/ra/settings/hazardcategories/').push().key;

		// Write the new hazard data
		var updates = {};
		updates['/ra/settings/hazardcategories/' + newKey] = newData;
		
		// write to database
		firebase.database().ref().update(updates);
		
		//renderDepartments();
		
		console.log("added");	
		
}

function addFrequency() {
	
		var currentAuthor = firebase.auth().currentUser.email;
		var currentTime = moment().format('D MMM YYYY, HH:mm');
		
		var newData = { 
			'author': currentAuthor,
			'lasteedited': currentAuthor,
			'created': currentTime,
			'updated': currentTime,
			'deleted': false,
			'description': $('#frequencyTitle').val()
		}
	
		// Get a key for a new hazard
		var newKey = firebase.database().ref('/ra/settings/frequencies/').push().key;

		// Write the new hazard data
		var updates = {};
		updates['/ra/settings/frequencies/' + newKey] = newData;
		
		// write to database
		firebase.database().ref().update(updates);
		
		//renderDepartments();
		
		console.log("added");	
		
}


function addScores() {
	
		console.log("about to write to " + key)
	
		var currentAuthor = firebase.auth().currentUser.email;
		var currentTime = moment().format('D MMM YYYY, HH:mm');
		
		var newData = { 
			'lastedited': currentAuthor,
			'updated': currentTime,
		}
		
		var scoreData = {
			'1': { 'description': $('#description1').val(), 'score': $('#score1').val() },
			'2': { 'description': $('#description2').val(), 'score': $('#score2').val() },
			'3': { 'description': $('#description3').val(), 'score': $('#score3').val() },
			'4': { 'description': $('#description4').val(), 'score': $('#score4').val() },
			'5': { 'description': $('#description5').val(), 'score': $('#score5').val() }
		}
		

		// Write the new data
		var updates = {};
		updates['/ra/settings/hazardcategories/' + key + '/scores'] = scoreData;
		
		// write to database
		firebase.database().ref().update(updates);
		
		console.log("wrote to " + key)
		
}



function editScores(activeKey) {
	
	key = activeKey;
	
	console.log("clicked on key " + key)
	
	$('#description1').val('');
	$('#description2').val('');
	$('#description3').val('');
	$('#description4').val('');
	$('#description5').val('');
	
	$('#score1').val('');
	$('#score2').val('');
	$('#score3').val('');
	$('#score4').val('');
	$('#score5').val('');
	
	firebase.database().ref('/ra/settings/hazardcategories/' + key + '/scores').once('value').then(function(snapshot) {
		var scores = snapshot.val();
		var counter = 0;

		if ( scores ) {
			$.each(scores, function(index, value) {
				counter = counter + 1;
				if ( scores[counter].description != null && scores[counter].score != null ) {
					$('#description' + counter).val(scores[counter].description);
					$('#score' + counter).val(scores[counter].score);
				}
				
			});
		};
		
	

	});
	
	
}
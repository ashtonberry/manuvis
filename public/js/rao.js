var deptkey = getURLParameter("department");

window.onload = function() {
  renderPage();
};


$(document).on('click', '#deptbut', function () { renderDepartments() } );
$(document).on('click', '#allass', function () { renderAssessments() } );
$(document).on('click', '#adddept', function () { addDepartment() } );
$(document).on('click', '#submitAss', function () { addAss() } );
$(document).on('click', '#editdocument', function() { editDocument() } );
$(document).on('click', '.controls.three', function() { undoMode() } );
$(document).on('click', '#closeeditBar', function() { $('#editBar').toggleClass('minimised'); } );


function editDocument() {
	
	$('#editBar').removeClass('delayed');
	$('.editcontrol').removeClass('delayed');
	
	
	if ( $('#undoBar').hasClass('undoon') ) {
		$('#editBar').addClass('delayed');
		$('.editcontrol').addClass('delayed');
		undoMode();
	}
	
	$('.controls.one').toggleClass('hidden');
	$('.controls.two').toggleClass('hidden');
	$('.controls.three').toggleClass('hidden');
	
	$('#editBar').toggleClass('editon');
	$('.editcontrol').toggleClass('active');
	//$('#closeeditBar').toggleClass('minimised');
	
	//$("#editBar").animate({width:'toggle'},350);
	
}

function undoMode() {
	$('#undoBar').toggleClass('undoon');
}


function renderPage() {
	
	firebase.database().ref('/ra/assessmentslist/').once('value').then(function(snapshot) {
		
		var statistics = snapshot.val();

		// Count how many hazards there are
		var hazCount = 0;
		if ( statistics ) {
			$.each(statistics.list, function(index, value) {
				hazCount = hazCount + value.hazcount;
			});
		};

		
		$('#totalass').html('<div class="row"><span class="value animated bounceIn">' + statistics.total + '</span></div><div class="row"><span class="metric">total assessments</span></div>');
		$('#totalhaz').html('<div class="row"><span class="value animated bounceIn">' + hazCount + '</span></div><div class="row"><span class="metric">hazards assessed</span></div>');
		
		
		
		
		// $('#columns').columns({
			  // data: theList,
			  // schema: [
                  // {"header":"Title", "key":"Title"},
                  // {"header":"Hazards", "key":"Hazards"},
                  // {"header":"Actions", "key":"Actions"},
              // ]
		  // });


		
	});
	
	var chart = c3.generate({
		data: {
			x: 'x',
	//        xFormat: '%Y%m%d', // 'xFormat' can be used as custom format of 'x'
			columns: [
				['x', '2013-01-01', '2013-01-02', '2013-01-03', '2013-01-04', '2013-01-05', '2013-01-06'],
	//            ['x', '20130101', '20130102', '20130103', '20130104', '20130105', '20130106'],
				['Approved', 30, 200, 100, 400, 150, 250],
				['Assessments', 130, 340, 200, 500, 250, 350]
			]
		},
		axis: {
			x: {
				type: 'timeseries',
				tick: {
					format: '%Y-%m-%d'
				}
			}
		}
	});

	setTimeout(function () {
		chart.load({
			columns: [
				['Hazards', 400, 500, 450, 700, 600, 500]
			]
		});
	}, 1000);

}

function addAss() {

	firebase.database().ref('/ra/assessmentslist/total/').once('value').then(function(snapshot) {
		
		console.log(snapshot.val());
		
		var currentAuthor = firebase.auth().currentUser.email;
		var currentTime = moment().format('D MMM YYYY, HH:mm');
		
		var newData = { 
			'title': $('#raTitle').val(), 
			'departments': $('#raDept').val(), 
			'deleted': false,
			'created': currentTime,
			'updated': currentTime,
			'hazcount': 0
		}
	
		// Get a key for a new hazard
		var newKey = firebase.database().ref('/ra/').child('assessments').push().key;

		// Write the new hazard data
		var updates = {};
		updates['/ra/assessments/' + newKey] = newData;
		//updates['/ra/assessments/' + assessment + '/updated/'] = currentTime;
		//updates['/ra/assessments/' + assessment + '/lastedited/'] = currentAuthor;
		updates['/ra/assessmentslist/list/' + newKey] = newData;
		
		
		
		//updates['/ra/assessmentslist/list/' + newKey + '/hazards'] = 0;
		updates['/ra/assessmentslist/total'] = snapshot.val() + 1;
		
		// write to database
		firebase.database().ref().update(updates);
		
		//renderHazards();
		
		
		
	});
			
		
		
		
		
}




// Render steps
function renderDepartments() {
			
	firebase.database().ref('/departmentlist/').once('value').then(function(snapshot) {
		
		var departments = snapshot.val();
		
		$('#list').html('');
		$('#listhead').html('\
			<tr>\
				<th>Department</th>\
				<th class="text-center">Assessments</th>\
				<th>&nbsp;</th>\
			</tr>');
		
		if ( departments ) {
			$.each(departments, function(index, value) {
				$("#list").append('\
					<tr data-dept="' + index + '">\
						<td>\
							<span class="text-main text-semibold">' + value.title + '</span>\
							<br>\
							<small class="text-muted">Last updated: ' + value.updated + '</small>\
						</td>\
						<td class="text-center">' + value.assessments + '</td>\
						<td class="text-center"><i class="ion-plus-round tablecontrols"></i><i class="ion-edit tablecontrols"></i><i class="ion-close tablecontrols"></i></td>\
					</tr>\
				');
			});
			
			
		$("#list").append('\
			<tr>\
				<td>\
					<a data-remodal-target="addDept" href="#">\
						<button class="raocontrol one small animated bounceIn" id="adddept">\
							<i class="ion-plus-round"></i>\
						</button><span>add new department</span>\
					</a>\
				</td>\
				<td class="text-center">test</td>\
				<td class="text-center">test</td>\
			</tr>\
		');
			
		// IF THE USER HAS REACHED THIS PAGE AND IS TARGETTING A SPECIFIC DEPARTMENT, RENDER THE PACKS FROM THAT DEPARTMENT IMMEDIATELY
		// Caution: if renderDepartments is run a second time, this will trigger again
		// Currently cancelling the above by reloading the page :P
		if ( deptkey ) {
			$( "tr[data-dept='" + deptkey + "']" ).addClass("active");
			renderPacks(deptkey);
		}
			
		} else {
			//$("#packList").append("<tr><td>" + "No packs exist." + "</td></tr>");
			//$("#departmentList").append("<tr><td>" + "No packs exist." + "</td></tr>"); 
			//$("#statusList").append("<tr><td>" + "No packs exist." + "</td></tr>"); 
			//$("#dateList").append("<tr><td>" + "No packs exist." + "</td></tr>"); 
			//$("#updateList").append("<tr><td>" + "No packs exist." + "</td></tr>"); 
		}
		
		

		
	});

};


function showAllAssessments() {
	
		firebase.database().ref('/ra/assessmentslist/').once('value').then(function(snapshot) {
		
		var assessments = snapshot.val();
		
		//$('#columns').columns('destroy');
		//$('#columns').html('');
		// $('#listhead').html('\
			// <tr>\
				// <th>Department</th>\
				// <th class="text-center">Hazards</th>\
				// <th>&nbsp;</th>\
			// </tr>');
		
		// if ( assessments ) {
			// $.each(assessments, function(index, value) {
				// $("#list").append('\
				// \
					// <tr data-ass="' + index + '">\
						// <td>\
							// <span class="text-main text-semibold">' + value.title + '</span>\
							// <br>\
							// <small class="text-muted">Last updated: ' + value.updated + '</small>\
						// </td>\
						// <td class="text-center">' + value.hazards + '</td>\
						// <td class="text-center"><i class="ion-plus-round tablecontrols"></i><i class="ion-edit tablecontrols"></i><i class="ion-close tablecontrols"></i></td>\
					// </tr>\
				// \
				// ');
			// });
		
		var theList = [];

		// Count how many hazards there are
		var hazCount = 0;
		if ( assessments ) {
			$.each(assessments.list, function(index, value) {
				hazCount = hazCount + value.hazcount;
				theList.push(
				{ 
					"Title": '<a href="view.html?assessment=' + index + '"><span class="text-main text-semibold">' + value.title + '</span></a><br><small class="text-muted">Last updated: ' + value.updated + '</small>', 
					"Hazards": value.hazcount,
					"Actions": '<a href="view.html?assessment=' + index + '"><i class="ion-eye tablecontrols"></i></a><a href="view.html?assessment=' + index + '#addHazard"><i class="ion-plus-round tablecontrols"></i></a><i class="ion-edit tablecontrols"></i><i class="ion-close tablecontrols"></i>',
					"key": index
				});
			});
		};

		
		$('#columns').columns({
			  data: theList,
			  schema: [
                  {"header":"Title", "key":"Title"},
                  {"header":"Hazards", "key":"Hazards"},
                  {"header":"Actions", "key":"Actions"},
              ],
			  sortBy: 'Title',
			  size: 10
		  });
			
			
			
		
		$('.ui-table-search').attr('placeholder', 'quick search by typing in the risk assessment title').addClass("u-full-width");
		// $("#columns tbody").append('\
			// <tr>\
				// <td>\
					// <a data-remodal-target="addAss" href="#">\
						// <button class="raocontrol one small animated bounceIn" id="addass">\
							// <i class="ion-plus-round"></i>\
						// </button><span>add new risk assessment</span>\
					// </a>\
				// </td>\
				// <td class="text-center"></td>\
				// <td class="text-center"></td>\
			// </tr>\
		// ');
		
	});
	
}

function renderAssessments() {

	showAllAssessments();

};


function addDepartment() {
	
	//console.log($('#enterrisk').val());
	
		var currentAuthor = firebase.auth().currentUser.email;
		var currentTime = moment().format('D MMM YYYY, HH:mm');
		
		var newData = { 
			'author': currentAuthor,
			'created': currentTime,
			'deleted': false,
			'title': "Climbing Harnesses"
		}
	
		// Get a key for a new hazard
		var newKey = firebase.database().ref('/departmentlist/').push().key;

		// Write the new hazard data
		var updates = {};
		updates['/departmentlist/' + newKey] = newData;
		
		// write to database
		//firebase.database().ref().update(updates);
		
		renderDepartments();
		
		
		
}

// START: PAGE LOAD

	$(document).ready(function($) {
		
		// tooltips
		$('.controls.one').tooltip({position: 'left center', content: 'add new risk assessment'});
		$('.controls.two').tooltip({position: 'left center', content: 'add new department'});
	
		showAllAssessments();

	});

// END: PAGE LOAD



// HELPER FUNCTIONS

// Pull parameters from URL
function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
};









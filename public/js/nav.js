$(document).ready(function($) {
	$('#nav').find('.nav-toggle').click(function(){

		// Expand or collapse this panel
		$(".nav-content").slideToggle('slow');

		// Hide the other panels
		//$(".nav-content").not($(this).next()).slideUp('slow');
	  
		// Change the click icon
		$( "#navbutton" ).toggleClass('ion-navicon', 'ion-close-circled');
		$( "#navbutton" ).toggleClass('ion-close-circled', 'ion-navicon');
		
		
		$('.navfooter').toggleClass('hidden');
	  
    });
	
	
	
	$(".scrolltop").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});
	
	$(window).scroll(function (event) {
		var scroll = $(window).scrollTop();
		if ( scroll > 0 ) {
			$('.scrolltop').removeClass('hidden');
		} else {
			$('.scrolltop').addClass('hidden');
		}
	});


});


